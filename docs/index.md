---
hide:
  - navigation
  - toc
---

# Docker Swarm

Orchestration de containers

---

Slides de la présentation:

- Version en ligne: [docker-swarm.html](docker-swarm.html)
- version PDF: [docker-swarm.pdf](docker-swarm.pdf)

---

Labs correspondants à cette session :

- [https://ebraux.gitlab.io/docker-labs/](https://ebraux.gitlab.io/docker-labs-swarm/)


---

![image alt <>](assets/docker-swarm-logo.png)




