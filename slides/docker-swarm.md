---
marp: true
paginate: true
theme: default
---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->
![bg left:30% 100% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/docker-swarm-logo.png)


# Docker Swarm
## Orchestration d'infrastructures Docker



-emmanuel.braux@imt-atlantique.fr-

---

# Licence informations


Auteur : -emmanuel.braux@imt-atlantique.fr-

Cette présentation est sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
Selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](img/cc-by-nc-sa.png)


---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 89% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/docker-swarm-logo.png)

# Docker Swarm
- **Principes et Concepts**
- **Architecture**
- **Cycle de vie des services**
- **Gestion du déploiement des services**
- **Swarm Config et et Swarm Secrets**
- **Gestion du réseau**
- **Swarm Logs et Swarm Events**
- **Opérations d'un cluster Swarm**
- **Sécurité d'un cluster Swarm**
- **Perspectives**

---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 89% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/docker-swarm-logo.png)


## > Principes et Concepts
- **Architecture**
- **Cycle de vie des services**
- **Gestion du déploiement des services**
- **Swarm Stacks**
- **Swarm Config et et Swarm Secrets**
- **Gestion du réseau**
- **Swarm Logs et Swarm Events**
- **Opérations d'un cluster Swarm**
- **Sécurité d'un cluster Swarm**
- **Perspectives**

---

# Docker

- Déployer simplement des applications
- Indépendamment de l'environnement


# Docker Compose
- Simplifier la gestion de l’exécution des conteneurs
- Gérer une application multi-conteneurs
- Gérer les ressources associées à un conteneur


---
# Orchestration 

- Gérer les **ressources** dans un cluster de serveur
  - Les ressources sont opérationnelles ?
  - Les ressources s’exécutent la où elle doivent le faire ?
  - ...
- Gérer le **cycle de vie des applications** :
  - Création / Suppression
  - Mise à jour, ...
  - Dimensionnement 
  - ...

---
# Orchestration 

- Gérer les **cluster de serveurs**
  - Ajouter / supprimer des serveurs
  - Assurer la disponibilité
  - Assurer la redondance des services
  - ...

#  **... Automatiser**



---
 # Dans l'écosystème Docker

- Gérer les conteneurs 
- Gérer les ressources associées aux conteneurs :
  - Les réseaux
  - Les volumes
  - Les secrets
  - ...


---
# Docker Swarm

***Objectif :*** Créer des **clusters de machines** exécutant des **conteneurs Docker** et fonctionnant ensemble **comme une seule machine.**

- **Simplifier** au maximum le déploiement et l’administration d’un cluster
- Des fonctionnalités **limitées**, mais **suffisantes**
- S'appuyer sur Docker et surtout Docker Compose
  
---
# Historique 

## Docker "Classic" Swarm :
- Projet natif de clustering de Docker 
- Lancé en 2014, arrêté en 2018
- Très limité (execution de commandes sur plusieurs noeuds à la fois)
- Projet archivé, le développement n'est plus actif
  
## Swarm Kit
  - Fonctionnalités de gestion de cluster et d'orchestration pour systèmes distribués
  - Projet Indépendant de Docker Swarm
  
---

## Docker Swarm 
![bg left:30% 89% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/docker-swarm-logo.png)

- "Swarm Kit" Intégré à "Docker Engine"
- Depuis la version 1.12 (2016)

## Docker Engine, fonctionnant en "Swarm Mode"

---
# Principales fonctionnalités

- Intégré **nativement** à "Docker Container Engine"
- **Modèle déclaratif** : Déclaration de l'*état désiré* des services
- **Reconciliation** : Swarm réalise les modifications nécessaires pour passer de l'état courant à l'état désiré
- **Scaling** : Up ou Down des services
- **Rolling Update** : Mise à jour progressive des applications, et retour en arrière, ...

---
# Principales fonctionnalités 

- **Architecture décentralisée** : Chaque noeud peut devenir Manager
- Gestion de **réseau Multi-Host** : Virtualisation de réseau entre les noeuds
- **Service Discovery** : Gestion automatique de l'accés aux services
  - Gestion des noms DNS
  - Load Balancer pour les accès externes, et le fonctionnement interne. 
- **Sécurisation** : Échanges entre noeuds chiffrés


---
# Cluster Swarm

- Système Distribué : **Manager(s)** / **Worker(s)**
- Tout type de machine : physique, VM, cloud, ...
- **Manager Nodes** : Noeuds chargés de la gestion du cluster
- **Worker Nodes** : Noeuds chargés d'exécuter la charge de travail

---
# Création d'un cluster Swarm 

Initialisation du cluster : 
``` bash
docker swarm init
```

Ajout d'un noeud
``` bash
docker swarm join <TOKEN>
```

---
# Déploiement 
![bg left:45% 95%](img/swarm-service-diagram.png)

- **Service** : 
  - Structure principale de Swarm

- **Notion de "réplica"**

- **Conteneur** :
  - Une unité d'execution
  
- **Tâche** :
  - Un conteneur associé à un contexte

---
# Terminologie

- **Service** (Service):  
  - Description de l'état désiré pour une application
  - Définition des tâches à exécuter
  - Configuration/paramétrage (nombre de réplicas, limites de ressources, ...)
- **tâches** (Task) : 
  - Un conteneur Docker
  - Les commandes à exécuter sur ce container
  - Les éventuelles options d'exécution

---
# Lancement d'un service

Lancement d'un service
``` bash
docker service create --replicas 2 <IMAGE>
```

Lister les services
``` bash
docker service ls
```

Lister les tâches d'un service
``` bash
docker service  ps <SERVICE_NAME>
```

---



<!-- _class: invert -->
<!-- _color: white -->

# LAB "Principes et Concepts de DockerSwarm"
- Créer un "Single Node" Swarm Cluster
- Créer un service
- Tester la Réconciliation

---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 89% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/docker-swarm-logo.png)


- ***Principes et Concepts***
## > Architecture 
- **Cycle de vie des services**
- **Gestion du déploiement des services**
- **Swarm Stacks**
- **Swarm Config et et Swarm Secrets**
- **Gestion du réseau**
- **Swarm Logs et Swarm Events**
- **Opérations d'un cluster Swarm**
- **Sécurité d'un cluster Swarm**

---  
# Terminologie

- **Swarm** : Le cluster Swarm
- **Noeud** (Node) : Instance de Docker, faisant partie du cluster Swarm
- **Manager Nodes** : 
  - Noeuds chargés de la gestion du cluster
  - Contrôle, supervision, ...
  - Au moins un Manager, plusieurs possibles.
  - Mais un seul Leader

---
# Terminologie

- **Worker Nodes**
  - Noeuds chargés d'exécuter la charge de travail
  - Aucune tâche de gestion du cluster
  - Interagissent avec le Manager
    - Interrogent le Manager pour obtenir des tâches à traiter
    - Informent le Manager de leur état

---
# Architecture

![h:400 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/swarm-diagram.png)   

>Configuration du cluster dupliquée (Raft Logs / Database)
---
# Organisation des Managers Nodes

- Un Manager peut agir comme un Worker.
- Chaque noeud peut devenir un Manager, ou un Worker
- Leader: 
  - Responsable de la planification des tâches
  - Mécanisme d'élection parmi les managers
- Nombre impair de Manager Node : 
  - Tolérance de panne (N-1)/2
  - Pour avoir le quorum : accord de (N/2)+1

---
# Quelques recommendations

- En général un "Manager Node" est uniquement un "Manager"
- Un Manager doit être stable.
- Un Manager ne doit pas être contraint en ressources
- Si utilisation avec des VMs : réserver des ressources

  
---
# Raft Consensus 

Docker Swarm s'appuie sur le mécanisme "Raft Consensus"
- S'assurer que tous les noeuds gérant le cluster sont consistants
- Si un manager tombe en panne, un autre prend le relais
  - Sans perte d'information,
  - Sans perturbation du cluster
- Raft logs :
  - Chaque Manager Node dispose d'une copie identique des logs
  - Contiennent les "credentials", certificats TLS, Docker secrets, ...

---

<!-- _class: invert -->
<!-- _color: white -->

# LAB "Architecture de Docker-Swarm"
- Créer un cluster de 3 serveurs
- Déployer un outil de visualisation du cluster

---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 89% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/docker-swarm-logo.png)

- ***Principes et Concepts***
- ***Architecture***
## > Cycle de vie des services
- **Gestion du déploiement des services**
- **Swarm Stacks**
- **Swarm Config et et Swarm Secrets**
- **Gestion du réseau**
- **Swarm Logs et Swarm Events**
- **Opérations d'un cluster Swarm**
- **Sécurité d'un cluster Swarm**
- **Perspectives**
  
---
# Déploiement des services

Lancement d'un service
``` bash
docker service create --replicas 2 <IMAGE>
```

Lister les services
``` bash
docker service ls
```

Lister les tâches d'un service
``` bash
docker service  ps <SERVICE_NAME>
```
---
# Options de création d'un service

Beaucoup d'options disponibles :
- Options de gestion du cycle de vie : update, rollback
- Option de déploiement : labels, contraintes, préférences, ...
- Utilisation des ressources : limites, reservations, 
- Configuration réseau : réseaux existants, publication de ports, ...
- ...

*[https://docs.docker.com/engine/reference/commandline/service_create/](https://docs.docker.com/engine/reference/commandline/service_create/)*

---
# Mise à jour des services

## Principales opérations faites sur un cluster

Dans la plupart des cas : **mise à jour des images**
- Nouvelle version de l'application déployée, ou d'un des composants utilisés
- Mise à jour de sécurité
- ...

Entraîne le **remplacement** des conteneurs existants par de nouveaux, et donc potentiellement  Un arrêt de service de l'application : **"DownTime"**.

---
# Commandes de modification

- Changer l'image utilisée par un service (nouvelle image disponible)
`docker service update --image myapp:1.2.1 <SERVICE_NAME>`
- Changer le nombre de réplicas de services
`docker service scale web=6 api=4`
- Ajouter ou supprimer une variable d'environnement
`docker service update --env-add MY_VAR=newvalue <SERVICE_NAME>`
- Ajouter ou supprimer un port publié
`docker service update --publish-rm 8080 <SERVICE_NAME>`

> Syntaxe générique : <***option***> + "***-add***" ou "***-rm***"

---
# Objectif : zéro DownTime

Par défaut, l'update remplace tous les réplicas, un par un.

De nombreuse options disponibles pour modifier ce fonctionnement
- Cas simples : possible de ne pas avoir de downtime
  - Serveurs web, ...
- Cas complexe : limiter le downtime au maximum
  - Base de données, ...

---
# Workflow d'un service update

- Swarm recherche un noeud pour héberger le conteneur : **Pending**
- Préparation du noeud retenu :  **Preparing**
  - configuration du réseau, chargement de l'image, ...
- Puis création du container (sans le démarrer) : **Ready**
- Si un problème sur ces étapes: la tâche est supprimée, une autre tâche est lancée.
- Si tout s'est bien passé : 
  - Arrêt de la tâche à remplacer *("desired state" = shutdown)*
  - Puis lancement de la nouvelle tâche:  **Running**
 
---
#   Paramétrage du Workflow

- Nombre de réplicas à remplacer en parallèle : `update-parallelism`
- Délai entre chaque opération de remplacement d'une tâche : `update-delay` 
  - secondes **Ts**, minutes **Tm**,  heures **Th**. ex 1m30s
- Action à effectuer en cas d'erreur : `--update-failure-action`
  - "pause" ou "continue" *(utilisés en test)*
  - "rollback" *(utilisé en production)*
- Nombre de tâches qui peuvent être en échec : `update-max-failure-ratio`
  - Par défaut : 0
  - ".25" = 25% de tâches peuvent échouer

---
#   Paramétrage du Workflow

- Si ta tâche d'origine ne répond pas, l'arrêt est forcé. Le temps d'attente peut être configuré : `--stop-grace-period`
- S'il n'y a pas de contraintes de cohabitation (file multi-access) l'option `--update-order start-first` permet de lancer la nouvelle tâche, et d'arrêter l'ancienne ensuite.
- Pour s'assurer s'assurer qu'une tâche fraîchement déployée est stable : durée pendant laquelle Swarm va monitorer une tâche `--update-monitor`
- ...

*[https://docs.docker.com/engine/reference/commandline/service_update/](https://docs.docker.com/engine/reference/commandline/service_update/)*

---
# Utiliser le HealthCheck

Les options de "HealthCheck' des conteneurs sont disponibles pour les services :

- Commande à lancer : `--health-cmd`
- Durée entre 2 vérifications : `--health-interval`
- Nombre de tentatives infructueuses tolérées : `--health-retries`
- Durée d'initialisation du container, sans verification  : `--health-start-period`
- Durée Max d'exécution d'une vérification : `--health-timeout duration`

Le healthcheck peut être désactivé : `--no-healthcheck`

---
# Ajuster les options de rollback

Le fonctionnement du Rollback peut être adapté avec les mêmes critères que l'update :

- Nombre de tâches à lancer en parallèle : `--rollback-parallelism`
- Délai entre chaque opération de rollabck : `--rollback-delay`
- Action en cas de problème sur le Rollback *("pause"|"continue")* : `--rollback-failure-action`
- Nombre de tâches qui peuvent être en échec : `--rollback-max-failure-ratio`
- Stabilité du Rollback : `--rollback-monitor`
- Odre d'arrêt/démarrage des tâches *("start-first"|"stop-first")* : `--rollback-order`

---
# Recommendations

- Bien connaître le fonctionnement des applications
  - Temps de démarrage
  - Temps d'arrêt
  - Mécanismes de gestion des sessions
  - ...
- Maîtriser les timeout réseau
  - Le timeout réseau doit être adapté à la durée des commandes


---

<!-- _class: invert -->
<!-- _color: white -->

# LAB "Cycle de vie des services"
- Déployer et le redimensionner un service 
- Mettre à jour l'image utilisée par le service
- Effectuer un rollback


---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 89% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/docker-swarm-logo.png)

- ***Principes et Concepts***
- ***Architecture***
- ***Cycle de vie des services***
## > Gestion du déploiement des services
- **Swarm Stacks**
- **Swarm Config et et Swarm Secrets**
- **Gestion du réseau**
- **Swarm Logs et Swarm Events**
- **Opérations d'un cluster Swarm**
- **Sécurité d'un cluster Swarm**
- **Perspectives**

---
# Mode de déploiment par défaut

- Docker Swarm gére la répartition de conteneurs sur les noeuds du cluster
  - Lancer les conteneurs
  - S'assurer que le nombre de réplicas demandé sont lancés.
- Stratégie de déploiement : "**spread**" uniquement.
   - Evaluation des ressources disponibles sur les noeuds du cluster
   - Selection de noeuds pour le déploiement
   - Répartition uniforme des tâches sur les noeuds disponibles
   - 1 container par noeud si possible.

> *Les stratégies "binpack" et "random" étaient liées au Docker Classic swarm"*

---
# Contrôle du déploiement

Plusieurs leviers : 

- Les ressources disponibles (CPU/RAM) : **Resource Requirements** 
- Utilisation de tags <key>=<value>
  - Bloquant :  **Service Constraints**
  - Non bloquant : **Placement Preferences**
- Mode  replicated / global :  **Service Modes**


---
# Resource Requirements

- Utilisable à la création, ou lors d'un update
- Concernent uniquement CPU et Mémoire
- Réservation et limitation
- S'applique sur chaque conteneur déployé, pas globalement au service

---
## **Resource Requirements**
# Limitation

``` bash
docker service create \
  --limit-memory 256M --reserve-cpu .5 \
  mysql
```
- CPU : 
  - real Core, hyperthreaded : comme vu au niveau OS (htop)
- Mémoire :
  - Si la limite est atteinte, une exception "Out of memory" est déclenchée
  - **Le container est tué et re-planifié**.

---
## **Resource Requirements**
# Réservation

``` bash
docker service create \
  --reserve-cpu .5 --reserve-memory 128M \
  mysql
```
- Réservation au niveau de Swarm
- **Pas en lien avec l'usage réel** des ressources au niveau OS
- Information prise en compte dans le processus de "scheduling"
- La reservation est **bloquante**

---
## **Resource Requirements**
# Modification, suppression

Modifier une limite / réservation : 
``` bash
docker service update --limit-memory 128M --reserve-cpu .25  ex-limit
```

Enlever une limite / réservation :
``` bash
docker service update --limit-memory 0 --reserve-cpu 0  ex-limit
```

---
# Service Constraints (+ Node Labels)

- Basé sur un mécanisme de "Labels"
  - Pré-définis dans Docker
  - Personnalisés 
- Si pas de noeud correspondant à la contrainte :
  - Pas de déploiement
  - La tâche reste en mode "pending"
- A la création, ou lors d'un update

---
## **Service Constraints**

- Plusieurs contraintes possible pour un service
- Basé sur une clé :
  - Existe ou non
  - Evaluation clé/valeur, opérateur `==` ou `!=`

On ne peut pas modifier une contrainte. Uniquement ajouter ou supprimer

---
## **Service Constraints**
# Labels Types

- **node.labels** :
  - défini via une commande swarm
- **engine.labels** :
  -  Défini dans le fichier de configuration daemon.json
     - `{"labels":["gpu=true"]}`
  - Plus complexe à mettre en place : fichier de configuration du daemon + restart

---
## **Service Constraints**
# Labels : bonnes pratiques d'utilisation : 
- Utiliser des node.labels de préférence
- Utiliser les engine.labels si : 
  - Pas d'accès à swarm possible
  - Contraintes fortes liée au déploiement de l'architecture (DMZ, ...)
  - Automatisation du déploiement des noeuds Docker (pré configuration)


---
## **Service Constraints**
# Label "Built-in"

- **node.id**
- **node.hostname**
- **node.ip**
- **node.role** : manager | worker
- **node.platform.os** : linux | windows | etc...
- **node.platform.arch** : x86_64 | arm64 | 386 | etc...
- **node.labels** ! vide par défaut

*[https://github.com/moby/swarmkit](https://github.com/moby/swarmkit)*

---
## **Service Constraints**

**Déploiement uniquement sur un manager**
-  Utilisation du "built-in" label "node.role", 2 alternatives : 
- `docker service create --constraint node.role==manager nginx`
- `docker service create --constraint node.role!=worker nginx`

**Ajout d'un label sur un noeud**
- `docker node update --label-add=ssh=true node2`

**Déploiement d'un service sur un noeud spécifique**
- `docker service create --constraint node.labels.ssh==true nginx`


---
# Placement Preferences

- Équivalent à une contrainte, mais **non bloquante**.
  - Si aucun noeud ne correspond à la préférence, les conteneurs sont tout de même exécutés, au mieux.

- Stratégie supportée : "**spread**" uniquement (mais nécessaire dans la commande)

- Utilisable à la création, ou lors d'un update

- Supporte multiple "Placement Préférences" 

---
## **Placement Preferences**
# Gestion des labels

- Nécessite des labels définis sur tous les noeuds, sinon la valeur du label = NULL
- Exemple d'utilisation, avec le label "rack" :
``` bash
docker service create \
    --placement-pref 'spread=node.labels.rack' \
    nginx
```

> Attention, pas de redéploiement automatique en cas de modification de labels

---
# Service Modes

- **replicated** :
  - X conteneurs déployés sur les noeuds, en fonction du nombre de réplicas demandés
  - Mode par défaut
- **global** :
  - Un conteneur par noeud
  - Le service sera déployé automatiquement sur tous les nouveau noeuds ajoutés au cluster.
---
## **Service Mode**
#  Utilisation

- Défini uniquement lors de la création du service. Pas de modification possible.
- Peut être associé à des contraintes : 
  - Un conteneur par noeud, mais uniquement sur une certaine catégorie de noeuds

Utilisation : 
`docker service create --mode=global nginx`

---
## **Service Modes**
# Cas d'usage pour "Global"

- Équivalent à "un agent sur chaque noeud du cluster"
- Outil de sécurité, ou de monitoring, collecte de logs
- Backups
- Proxy
- ...

---

<!-- _class: invert -->
<!-- _color: white -->

# LABs "Gestion du déploiement des services"
- Déployer un service en utilisant 
  - les "Resource requirements"
  - les "Service Constraints"
  - les "Placement Preferences"
  - les "Service Modes"


---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 89% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/docker-swarm-logo.png)

- ***Principes et Concepts***
- ***Architecture***
- ***Cycle de vie des services***
- ***Gestion du déploiement des services***
## > Swarm Stacks 
- **Swarm Config et et Swarm Secrets**
- **Gestion du réseau**
- **Swarm Logs et Swarm Events**
- **Opérations d'un cluster Swarm**
- **Sécurité d'un cluster Swarm**
- **Perspectives**

---
# Stacks

Permet de manipuler un ensemble de services.

Rassemble dans un fichier yaml toutes les commandes applicables aux services
- Remplace les lignes de commande
- Fichier texte (documentation, backup, version, reproductibilité, ... )

Extension du format Compose (minimum version 3)

> Pas besoin d'installer docker-compose, ou le plugin "compose" pour Docker, dans un cluster Swarm.

*[https://docs.docker.com/compose/compose-file/compose-file-v3](https://docs.docker.com/compose/compose-file/compose-file-v3)*

---
# Différences avec **Compose**

## Ajout de l'option `deploy:` (ignorée par docker compose):
- Description du déploiement du service
- Nombre de réplicas, règles de planification, gestion en cas perte d'un conteneur, ...

## Mais de nombreux paramètres ignorés :
- Build des images et dépendances entre services
- Personnalisation des conteneurs
- Gestion de ressources "locales" à un noeud

*[https://docs.docker.com/compose/compose-file/compose-file-v3/#not-supported-for-docker-stack-deploy](https://docs.docker.com/compose/compose-file/compose-file-v3/#not-supported-for-docker-stack-deploy)*

---
# Paramètres compose ignorés : Build des images et dépendances entre services

- `build:` : Docker stack ne "build" pas d'image avant de les déployer
  - Inadapté dans un environnement de type cluster
  - Le build doit être fait en amont : tests, chaîne de CI, ...
- `depend-on`: 
  - Les service peuvent être relancé à tout moment
  - Les dépendances doivent être gérées dans la phase de démarrage des services (wait-for-it, ... )
  - *[https://docs.docker.com/compose/startup-order/](https://docs.docker.com/compose/startup-order/)*

---
# Paramètres compose ignorés : configurations avancées

- Personnalisation des conteneurs: 
  - Nom du conteneur : `container_name`
  - `restart` est remplacé par `restart_policy`
  -  Sécurité, isolation : capabilities / cgroups / Namespace  `cap_add`,`cap_drop`, `cgroup_parent`, `userns_mode`, ...
- Gestion de ressources "locales" à un noeud
  - `network_mode` (bridge, hosts),
  - `devices`, `sysctls`, 


---
# Principales commandes `docker stack`:

- ` docker stack deploy -c <compose-file> <stack-name>` : lancer un stack
   - Création des ressources, en background
   - Possibilité d'utiliser plusieurs fichiers compose
- `docker stack ls` : lister les stacks
- `docker stack services <stackname>` : lister les services associés 
- `docker stack ps <stackname>` : lister les les tâches associées
  - Afficher toutes les tâches lancées sur le cluster
  - *`docker container ps`* : affiche les conteneurs sur l'hôte uniquement

> Pour "update" utiliser "deploy" : si le stack existe, Swarm met à jour sa définition, et lance la reconciliataion.

---
# Exemple de fichier stack

- Utilisation du même format de description YAML que docker-compose
- Quelques champs spécifiques : 

```yaml
Version: "3"
services:

  memcached:
    image: memcached

    deploy:
      mode: replicated
      replicas: 3
      restart_policy:
        condition: any
      placement:
        max_replicas_per_node: 1
```

---
# Resource Requirements

``` yaml
    deploy:
      resources:
        limits:
          cpu: '0.5'
          memory: 256G
        reservations:
          cpu: '0.25'
          memory: 128G
```
> Différence avec le format docker-compose : les informations de "resource" doivent être dans une rubrique "**deploy**"

---
# Service Constraints 

``` yaml
    deploy:
      placement:
        constraints:
          - node.labels.disk == ssd
          - node.role == worker
```

# Placement Preferences

``` yaml
    deploy:
      placement:
        preferences:
          - spread: node.label.rack
```

---
# Service Modes

``` yaml
    deploy:
      mode: global
```

---

<!-- _class: invert -->
<!-- _color: white -->

# LAB "Docker-Swarm Stacks"
- Déployer un service en utilisant une stack

---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 89% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/docker-swarm-logo.png)

- ***Principes et Concepts***
- ***Architecture***
- ***Cycle de vie des services***
- ***Gestion du déploiement des services***
- ***Swarm Stacks***
## > Swarm Config et et Swarm Secrets
- **Gestion du réseau**
- **Swarm Logs et Swarm Events**
- **Opérations d'un cluster Swarm**
- **Sécurité d'un cluster Swarm**
- **Perspectives**

---
# Docker configs et Secrets

**Les configs et secrets sont basés sur le même principe**
- Gérer des données **externes** aux services
- Accessibles via un montage dans les conteneurs

**Différence lors de l'utilisation**
- Les **configs** sont stockées sur le **disque** des noeuds
- Les **secrets** sont stockés en **mémoire** sur les noeuds : plus sécurisés (volatile, accès plus complexe, ...)
  

---
# Principe 

- Stockage clé/valeur
  - Le nom du fichier est la clé de la config / secret
  - La valeur est stockéee dans le fichier.
- Types de données : chaine de caractère ou binaire
- Limitations :
  - Taille maximum 500ko
  - Spécifique à Docker Swarm : pas dans Docker Compose

> Les secrets / configs sont des attibuts d'un service service: en cas d'ajout ou suppression, le service est redéployé.

---
# Config ou secret ?

Docker configs : Tout type de fichier (configuration, ... )

Docker secrets : Toute données sensible, qui ne doit pas être diffusée.
- Identifiants, username/password
- Clés SSH
- Clés d'accès API
- Tertificats TLS
- ...

---
# Interêt des Secrets

- Fonction de gestion de secrets, **intégrée** à Docker Swarm
- Simple à utiliser, pas de composant ou configuration additionnels 


> Limité à Swarm : si besoin de partage avec d'autres applications, utiliser une solution externe (Hashicorp Vault, ...)

---
# Configs ou Volumes ?

Les configs pourraient correspondre à de "petits" volumes.

Mais les configs (et secrets) sont :
- Stockées dans les **Raft logs**
  - Haute disponibilité
  - Accessibles sur chaque noeud


---
# Création d'un secret / config

- Transfert des données vers un Swarm Manager (échange sécurisé par TLS)
- Stockage les "Raft Logs" (chiffré)
- Synchronisé entre les managers (réplication)
- Echanges entre les noeuds sécurisé par défaut (TLS)

---
# Utilisation d'un config/secret dans un service

- Transfert vers le noeud où s'execute le container devant y avoir accès
- Le secret est
  - Déchiffré et stoké sur **disque**
  - Rendu accessible au conteneur à la racine `/<config_name>`
- La config est  
  - Déchiffrée et stokée en **mémoire**
  - Rendue accessible au conteneur dans `/run/secret/<secret_name>`
 
> Les configs/secrets sont "assignés" à un service : seuls les conteneurs dépendants du service y ont accès.

---

# Format "short" et "long"

En Format short, aucune personnalisation possible
En format long: possibilité de personnalisation
- De l'emplacement et du nom
- Des droits d'accès (de type "unix")

``` bash
docker service create \
  ...
  --config site-conf-file1 \
  --config source=site-conf-file2,target=/etc/nginx/conf.d/site.conf,mode=0440 \
  ...
```   
> Principe identique pour les secrets

---
# Docker configs - Administration

- Créer une config depuis le contenu d'un fichier :
  `docker config create <CONFIG_NAME>  <FICHIER>`
- Créer une config en ligne de commande :
  `echo "[monitoring]\nstatus=active\n" | docker config create <CONGIF_NAME> -`
- Lister les configs : `docker config ls`
- Supprimer une config : `docker config rm  <SECRET_NAME>`

> Une config ne peut pas être supprimé tant qu'un service l'utilise

---
# Docker config - Utilisation

- Créer un service avec une config
``` bash
docker config create \
  --config source=<CONFIG_NAME>,target=<DESTINATION_IN_CONTAINER>
```
- Modifier une config
``` bash
docker service update \
  --config-rm <ACTUAL_CONFIG_NAME> \
  --config-add source=<NEW_CONFIG_NAME>,target=<DESTINATION_IN_CONTAINER> \
  <SERVICE_NAME>
```  
---
# Utilisation dans un stack

Ajout d'une catégoire `configs` 
``` yaml
version: "3.3"
services:
  myapp:
     ...
    configs: 
        - source: myapp.conf
          target: /etc/nginx/conf.d/default.conf
          uid: '1000'
          gid: '1000'
          mode: 0440
     ...

configs:
  myapp.conf:
    file:  ./nginx.conf
```

---
# External config

Une config peut être définie comme `external`. 
- Elle ne sera pas créée au lancement de la stack
- Elle doit pré-exister

```yaml
configs:
  myapp.conf:
     external: true  
```

---
# Docker Secret - Administration

- Créer un secret depuis le contenu d'un fichier :
`docker secret create <SECRET_NAME>  <FICHIER>`
- Créer un secret en ligne de commande :
`echo "MySeCReT" | docker secret create <SECRET_NAME> -`
- Supprimer un secret : `docker secret rm  <SECRET_NAME>`
- Lister les secrets : `docker secret ls`

---
# Docker config - Utilisation

- Créer un service avec un secret
``` bash
docker service create \
  --secret source=<SECRET_NAME>,target=<DESTINATION_IN_CONTAINER>
```
- Modifier une config
``` bash
docker service update \
  --secret-rm <ACTUAL_SECRET_NAME> \
  --secret-add source=<NEW_SECRET_NAME>,target=<DESTINATION_IN_CONTAINER> \
  <SERVICE_NAME>
```  

> Un secret ne peut pas être supprimé tant qu'un service l'utilise

---
# Utilisation dans un fichier stack

Ajout d'une catégoire `secrets` 
``` yaml
version: "3.1"

services:
  myapp:
    ...
    secrets:
      - myapp_password

secrets:
  myapp_password:
    file:  ./myapp_password_value.txt
```

> Nécessite compose format `version: "3.1"`

---
# External secrets

Un secret peut être défini comme `external`. 
- Il ne sera pas créé au lancement de la stack
- Il doit pré-exister

```yaml
secrets:
  myapp_passwd:
     external: true  
```
---

<!-- _class: invert -->
<!-- _color: white -->

# LAB "Configs et Secrets"
- Utiliser des config et secrets pour déployer un service
- Réaliser une rotation de secrets

---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 89% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/docker-swarm-logo.png)

- ***Principes et Concepts***
- ***Architecture***
- ***Cycle de vie des services***
- ***Gestion du déploiement des services***
- ***Swarm Stacks***
- ***Swarm Config et et Swarm Secrets***
## > Gestion du réseau
- **Swarm Logs et Swarm Events**
- **Opérations d'un cluster Swarm**
- **Sécurité d'un cluster Swarm**
- **Perspectives**

---
#  Overlay networks

**Swarm s'appuie sur les réseau "Overlay"**
- Virtualisation de réseau entre les noeuds Swarm
- Pas de déploiement réseau spécifique préalable
- Trafic de conteneur à conteneur
- Un service peut être connecté à plusieurs réseaux Overlay
- Un réseau Overlay est créé par défaut pour chaque stack
- Création manuelle :  `docker network create --driver overlay my-network`

> Swarm ne créée un réseau overlay sur un noeud uniquement si au moins un service l'utilise

---
# Ingress network

- Réseau Overlay spécifique
- Assure le routage du trafic de l'extérieur vers les noeuds Swarm
- Le réseau  Ingress est créé par défaut.
- Il peut être personnalisé (plage d'IP, route, MTU, ...)
  
---
# Accés aux services : Service Discovery

## Accès externes aux services : routage par port

- Gestion du routage applicatif par port : 
  - Chaque port est affecté à un et un seul service
- Les ports sont gérés globalement sur le cluster
 

## Interconnexion entre service 

- Pas besoin de publier les ports utilisés pour échanger entre services dans le même réseau.


---
# Routing Mesh

- Swarm gére automatiquement le routage des paquets vers les conteneurs
- Trafic entre conteneurs :
  - Les requêtes sont réparties entre les conteneurs gérant un service
- Accès depuis l'extérieur (Ingress)
  - Les requêtes sont routées vers les noeuds hébergeant le service
- S'appuie sur un module du noyau Linux : IPVS *(Internet Protocol Virtual Server)*

---
# Limitations

- Stateless : pas de suivi de cookie, ...
  - Pas d'assurance de retomber toujours sur le même conteneur
- TCP Load Balancer : pas d'information sur le nom DNS. 
  - Impossible d'utiliser le nom DNS de l'hôte dans le conteneur (sauf si le protocole le transmet)
- Solutions :
  - Remplacer le Routing Mesh par un load balancer externe : HA proxy ou Nginx
  - Docker Entreprise Edition : Intégre un Load Balancer plus évolué

---
# Routing Mesh : Trafic entre conteneurs

  - Une IP spécifique est affectée à chaque **service** : VIP
  - Dans le même réseau que les conteneurs
  - Associée à aucun conteneur
  - Swarm fait le travail de routing entre l'Ip de service et les conteneurs (equivalent d'un load Balancer)

---
# Routing Mesh : Trafic entre conteneurs

![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/network-mesh-service.png)

---
# Routing Mesh : Ingress

- Mécanisme "Swarm Load Balancer"
- Chaque **noeud** a un load Balancer associé à son IP adresse externe
- Quand une requête arrive sur un noeud Swarm via le Ingress Network :
  - Swarm défini les noeuds hébergeant le service
  - Aiguille la requêtes vers un de ces noeuds 
    - si le conteneur est disponible sur l'hôte contacté : routé vers le conteneur
    - si le service est sur un autre noeud : routé à travers le réseau virtuel
 
---
# Routing Mesh : Ingress

![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/network-mesh-ingress.png)

---
# Routing MESH : IPVS

- Le mécanisme de routing MESH s'appuie sur IPVS
- IPVS : Internet Protocol Virtual Server
  - Intégré au noyau linux
  - implémentation d'un load balancer de niveau 3 et 4 au niveau du Noyau Linux
  - NAT et routage direct
  - Persistance des connexion
  - Routage par poid
  - Plusieurs protocoles de répartition Round Robin, Least-Connection, ..

> Swarm utilise IPVS de façon limtée dans la version communautaire, mais de façon plus complète dans la version Enterprise

---
---
# Gestion du trafic réseau

2 Types de trafic réseau 
- Gestion du cluster (Control and management plane trafic) : 
  - Echanges entre les noeuds du cluster pour la gestion du cluster
- Trafic applicatif  (Application data plane trafic) :
  - Echange entre les conteneurs
  - Inteconnexion externes

Utilisation de 2 interfaces différentes : 
- ***--advertise-addr*** : définir le trafic de gestion du cluster
- ***--data-path-addr*** : définir le trafic applicatif
 
> Préciser l'adresse IP pour les Manager, mais l'interface pour les Worker 


---

<!-- _class: invert -->
<!-- _color: white -->

# LAB "Les réseau"
- Utiliser des réseau Overlay
- Accéder à un service grace à un port publié
- Mettre en place un Load Balancer externe



---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 89% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/docker-swarm-logo.png)

- ***Principes et Concepts***
- ***Architecture***
- ***Cycle de vie des services***
- ***Gestion du déploiement des services***
- ***Swarm Config et et Swarm Secrets***
- ***Gestion du réseau***
## > Swarm Logs et Swarm Events
- **Opérations d'un cluster Swarm**
- **Sécurité d'un cluster Swarm**
- **Perspectives**
---
# Swarm Logs

- Utilise le même mécanismes de logs des containers
- **Aggrégation** des logs de toutes les tâches sur tous les noeuds pour un service
- Accessibles depuis un Manager Node
- Logs par service / Par tâche
- Pas de stockage des logs,
- Pas de traitement, d'indexation, ou d'outil de recherche 
- Par défaut au format "json"
  
> En fonction du "logging driver" les logs ne sont pas forcément disponibles sur les noeuds, et donc pas accessibles.



---
# Accéder aux logs 

Afficher les logs pour un service
`docker service logs <SERVICE_NAME>`

Afficher les logs pour une tâche
`docker service logs <TASK_ID>`

Options utiles :
- `--no-trunc` : pas de coupure des logs
- `--raw` : suppression du formatage liè aux container
- `--tail xx` : affiche les xx dernières lignes
- `--follow` : affichage de slogs au fil de l'eau


---
# Docker events

- Trace chaque action de Swarm
  - service update", "network create", "container start", ...
- Possibilité de filtrer, de modifier le format
- Limité au 1000 derniers events
- Non persistant : si besoin de Conservation / Monitoring : 
  - Scripts périodiques avec export,
   - Outils externes de collecte comme "fluent Bit" 


---
# Docker events - Utilisation

Afficher les events à venir : `docker events`

**Scope** : 
- Sur un worker : "events" du noeud uniquement
- Sur un manager : "events" du noeud, "events" de gestion du cluster Swarm
- Filtrage avec `--filter scope=local` ou `--filter scope=local`


**De nombreuses options et filtres.**


---
# Docker events - Filtres

- `--since yyyy-mm-jj` : depuis une date
- `--since yyyy-mm-jjThh:mm:ss` : depuis une heure
- `--since xxhxxm`  : depuis une durée
- `--filter event=start` : de type "start"
- `--filter type=network` : sur des types d'objets
- ` --filter scope=swarm` : dans le scope Swarm

> Les filtres peuvent être combinés
> 
*[https://docs.docker.com/engine/reference/commandline/events/](https://docs.docker.com/engine/reference/commandline/events/)*

---
# Cas d'usage

## Observer / comprendre les workflow
 - Différentes étapes à la création d'un container :
   - Création du réseau
   - Création du container
   - Démarrage du container
   
 ## Comprendre les problèmes sur un conteneur
 - Informations de "healthcheck": commande status, ...
 - Gestion des ressources : déclenchement du OOM Killer
 

---

<!-- _class: invert -->
<!-- _color: white -->

# LAB "Logs et Events"
- Accéder au logs d'un service
- Afficher les events d'un cluster

---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 89% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/docker-swarm-logo.png)

- ***Principes et Concepts***
- ***Architecture***
- ***Cycle de vie des services***
- ***Gestion du déploiement des services***
- ***Swarm Stacks***
- ***Swarm Config et et Swarm Secrets***
- ***Gestion du réseau***
- ***Swarm Logs et Swarm Events***
## > Opérations d'un cluster Swarm
- **Sécurité d'un cluster Swarm**
- **Perspectives**
  
---
# Ajout / suppression de noeud

- **Ajout** d'un noeud : depuis le noeud à ajouter
``` bash
docker swarm join <TOKEN>
```
> Le rôle du noeud dépend du token utilisée : *"Manager Token"*, ou *"Worker Token"*.


- **Suppression** d'un noeud du cluster : depuis le noeud à supprimer
``` bash
docker swarm leave 
```
> Le noeud ne doit pas être un manager

---
#  Changement de rôle

- **Promotion** d'un noeud en tant que "Manager Node"
``` bash
docker node promote <NODE_NAME>
```

- **Reconfiguration** d'un noeud en tant que "Woker node Node"
``` bash
docker node demote <NODE_NAME>
```

---
# Disponbilité des Noeuds - Etats 

- **active** : éxecute les tâches en cours, accepte de nouvelles tâches
- **pause** : éxecute les tâches en cours, n'accepte pas de nouvelles tâches
  - Troubleshooting :  Attention, si un container s'arrête sur ce noeud, il sera re-schedulé sur un autre
- **drain** : re-distribue ses tâches en cours, n'accepte pas de nouvelles tâches
  - Maintenance
  - Doit être fait systématiquement avant d'arrêter/suprimer un noeud Swarm
  
> N'affecte que les conteneurs, pas les autres ressources (volumes, network, ...)

---
# Commandes de changement d'état

- `docker node update --availability pause node02`
- `docker node update --availability drain node02`
- `docker node update --availability active node02`


---
# Sauvegarde d'un cluster Swarm

- Données sensibles :  `/var/lib/docker/swarm`
  - Le Raft Log
  - Les Clés de chiffrage du Raft Log
  - les fichier d'état du cluster

- Pour faire un backup :
  - Arrêter Docker sur un Manager Node
  - Faire une archive du dossier `/var/lib/docker/swarm`
  - Relancer Docker

---
# Restauration

Pour restaurer le cluster à partir d'un backup :
- Arrêter Docker sur le futur Manager Node
- Supprimer le dossier `/var/lib/docker/swarm`
- Le restaurer depuis le backup 
- Relancer Docker
- Initialiser un nouveau cluster `docker swarm init --force-new-cluster`
- Vérifier la configuartion `docker service ls`, ...
- Ajouter les noeuds à ce nouveau cluster

---
# En pratique

Si les contraintes de taille de cluster, et de disponibilité des applications le permettent :
- Automatiser le déploiement du cluster
- Automatiser le paramètrage du cluster (Node Labels, ...)
- Utiliser des stacks pour déployer des services
- Utiliser des volumes, configs et secrets pour les données persistantes
- Conserver les informations dans un dépôt centralisé, si possible versionné
- Automatiser le redéploiement du cluster

---
# Mise à jour

- Mettre à jour Docker sur tous les noeuds.


---
# En cas de perte du Quorum

- Rappel : 
  - Tolérance de panne (N-1)/2
  - Pour avoir le quorum : accord de (N/2)+1
- Swarm ne sait pas résoudre la perte de Quorum seul
- Solution : choisir un manager, et recréer un cluster à partir de ce manager
- Commande identique à la restauration du cluster:
-  `docker swarm init --force-new-cluster`

> Pas de solution miracle pour "choisir" le manager node
---

<!-- _class: invert -->
<!-- _color: white -->

# LAB "Swarm Opérations"
- Reconfigurer des neouds du cluster
- Ajouter / supprimer des noeuds
- Modifier l'état des noeuds
- Effectuer un backup/restauration d'un cluster


---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 89% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/docker-swarm-logo.png)

- ***Principes et Concepts***
- ***Architecture***
- ***Cycle de vie des services***
- ***Gestion du déploiement des services***
- ***Swarm Stacks***
- ***Swarm Config et et Swarm Secrets***
- ***Gestion du réseau***
- ***Swarm Logs et Swarm Events***
- ***Opérations d'un cluster Swarm***
## > Sécurité d'un cluster Swarm
- **Perspectives**

---
# Traffic réseau

- Echanges de gestion du trafic : chiffré
- Overlay networks **non chiffré** par défaut
  - Pour activer : `--opt encrypted` 
  - Activable individuellement
  - IPSEC (AES) encryption
  - Attention à l'impact sur les performances

---
# Echanges entre Managers

Utilisation d'une PKI interne
- Lors du 'swarm init', génération d'un "root Certificate Authority (CA)"
  - Paire de clé utilisée pour les échanges entre les noeuds
  - "Manager Token" et "Worker Token"
- Chaque noeud dispose d'un certificat pour l'authentifier 
- Le renouvelement des certificats :
  - Est fait automatiquement tous les 3 mois (parametrable)
  - Peut être forcé : `docker swarm ca --rotate`

> Toutes données sont stockées dans le Raft Log
> Possible d'utiliser son propre root CA : option `--external-ca` lors du swarm init

*[https://docs.docker.com/engine/swarm/how-swarm-mode-works/pki/](https://docs.docker.com/engine/swarm/how-swarm-mode-works/pki/)*


---
# Sécurisation du Raft Logs

- Si un Manager Node est compromis : 
  - Accès aux clés de chiffrement des "Raft Logs"
  - Utilisation de l'outil `swarm-rafttool`
  - Accèder aux secrets, ...
- Protection des clés de chiffrement : "Autolock"  
  - Génération d'une "autolock key" pour chiffrer les clés de chiffrement des "Raft logs"
  - Cette clé est indispensable ensuite pour déverouiller le "Raft Log" au lancement du cluster

> Attention, le cluster ne peut plus redémarrer sans intervention manuelle
*[https://docs.docker.com/engine/swarm/swarm_manager_locking/](https://docs.docker.com/engine/swarm/swarm_manager_locking/)*

---
# Utilisation de "Autolock"

Lancement du cluster Swarm en mode "autolock": 
`docker swarm init --autolock`
> Génère une clé, à conserver

Activer le mode Autolock sur un cluster Swarm en cours de fonctionnement
`docker swarm update --autolock=true`
> Génère une clé, à conserver

Désactiver le mode Autolock sur un cluster Swarm en cours de fonctionnement
`docker swarm update --autolock=false`
> Nécessite de saisir la clé 


---
# Isolation des environnements

- Hébergement de plusieurs environnements dans un cluster : development, staging, production, ...
- Isolation possible en utilisant les contraintes

Recommendation : déployer des clusters différents

---

<!-- _class: invert -->
<!-- _color: white -->

# LAB "Sécurité"
- Activer "Autolock" sur le cluster, et tetser son fonctionnement


---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 89% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/docker-swarm-logo.png)

- ***Principes et Concepts***
- ***Architecture***
- ***Cycle de vie des services***
- ***Gestion du déploiement des services***
- ***Swarm Stacks***
- ***Swarm Config et et Swarm Secrets***
- ***Gestion du réseau***
- ***Swarm Logs et Swarm Events***
- ***Opérations d'un cluster Swarm***
- ***Sécurité d'un cluster Swarm***
## > Perspectives 

---
# 2018

- Plusieurs orchestreurs de conteneurs : Kubernetes, Swarm, Mesos, Nomad, ...
  - **Kubernetes** : Très populaire
  - **Swarm** : En perte de vitesse, évolutions mineures
  - **Mesos** : En perte de vitesse, Marché de niche (Very Big Data)
  - **Consul** : Encore balbutiant
  
---
# 2019 

- Rachat de Docker par Mirantis 
  - The **primary** orchestrator going forward is **Kubernetes**.
  - Mirantis ... expects to **support Swarm for at least two years**.

*[https://www.mirantis.com/blog/mirantis-acquires-docker-enterprise-platform-business/](https://www.mirantis.com/blog/mirantis-acquires-docker-enterprise-platform-business/)*

---
# 2020

- Here at Mirantis, we’re excited to announce our **continued support for Docker Swarm**
  - Swarm has a proven track record of running mission critical container workloads in demanding production environments
  - Customers want continued support of Swarm without an implied end date.
-  Mirantis will **continue to support Swarm**.
-  Mirantis will be **continuing to invest** in active Swarm development.
  
*[https://www.mirantis.com/blog/mirantis-will-continue-to-support-and-develop-docker-swarm/](https://www.mirantis.com/blog/mirantis-will-continue-to-support-and-develop-docker-swarm/)*


---
# 2021

- Swarm **and** Kubernetes are part of our **future** strategy
- There are **some cases** where **Swarm might be a better choice** and other cases where Kubernetes is.
- Mmake our customers successful, **no matter what orchestrator they're going to be on**, now and in the future.
- Introducing **Swarm-Only Mode for Mirantis Kubernetes Engine**


*[https://www.mirantis.com/blog/docker-swarm-webinar-qa-long-live-docker-swarm/](https://www.mirantis.com/blog/docker-swarm-webinar-qa-long-live-docker-swarm/)*
*[https://www.mirantis.com/blog/introducing-swarm-only-mode-for-mirantis-kubernetes-engine-3-5-0/](https://www.mirantis.com/blog/introducing-swarm-only-mode-for-mirantis-kubernetes-engine-3-5-0/)*

---
# 2022

- Pprovide the best enterprise support for Swarm for the **long term** ... **at least the next 3 years, and for as long as it makes sense for our customers**.
- We have more than **100 customers** utilizing Swarm for production workloads ... more than **10,000 nodes** spread across approximately **1,000 clusters**, supporting over **100,000 containers** orchestrated by Swarm.
- **the perfect simple alternative to Kubernetes.**

*[https://www.mirantis.com/blog/mirantis-is-committed-to-swarm/](https://www.mirantis.com/blog/mirantis-is-committed-to-swarm/)*

---
# En conclusion

- Kubernetes reste très populaire. Swarm est perçu comme "ringard".
- Mais dans les faits: 
  - Swarm est robuste, simple, stable. **Est largemment utilisé**
  - Kubernetes reste trop complexe : **"We don't need k8s"**
- Les entreprises utilisent les 2 : 
  - workload simple : Swarm
  - workload critiques : k8S
- Swarm simplifie la migration vers les microservices et le Cloud.

*[https://www.mirantis.com/blog/kubernetes-vs-swarm-these-companies-use-both/](https://www.mirantis.com/blog/kubernetes-vs-swarm-these-companies-use-both/)*


---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 89% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/docker-swarm-logo.png)


# Ressources

---


Configuration réseau :

- [https://docs.docker.com/network/overlay/](https://docs.docker.com/network/overlay/)
- [https://docs.docker.com/engine/swarm/networking](https://docs.docker.com/engine/swarm/networking/)
- [http://www.linuxvirtualserver.org/software/ipvs.html](http://www.linuxvirtualserver.org/software/ipvs.html)
- [https://docs.mirantis.com/mke/3.3/ops/administer-cluster/use-node-local-network-in-swarm.html](https://docs.mirantis.com/mke/3.3/ops/administer-cluster/use-node-local-network-in-swarm.html)

*[https://docs.docker.com/engine/swarm/how-swarm-mode-works/nodes/](https://docs.docker.com/engine/swarm/how-swarm-mode-works/nodes/)*